FROM node:16.13.0

# Create app directory
WORKDIR /usr/src/app

COPY . .
RUN yarn install
RUN yarn add nest

EXPOSE 8080